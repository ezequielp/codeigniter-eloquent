<div class="gray-bg" id="login-page">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name" style="color: #00e9ba;">Coq</h1>
            </div>
            <h2 style="font-size: 18px;">Codeingiter + Eloquent</h2>
            <hr>
            <p>Ingrese con su usuario y contraseña</p>
            <form action="<?php echo base_url(); ?>login/ingresar" class="m-t" role="form" method="POST">
                <div class="form-group">
                    <input name="username" type="email" class="form-control" placeholder="micorreo@correo.com" required="">
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Contraseña" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>

                <a href="#"><small>Olvidé la contraseña</small></a>
            </form>
            <p class="m-t"> Ezequiel Paolillo <small>V1.0 2018</small> </p>
        </div>
    </div>

</div>