<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {

    public function index($version){
        $this->load->library("migration");
        if(!$this->migration->version($version)){
              show_error($this->migration->error_string());
        }
    }
    
    function output($vista = null, $data = null){

        $this->load->view('layout/header');
        if($vista){
            $this->load->view($vista,$data);    
        }
        $this->load->view('layout/footer');
    }
    
}
