<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index($cuit = null)
	{
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            
            $crud->set_theme('bootstrap');

            $crud->set_table('depositos');

            $table = $crud->render();

            $output = array();

            $output['table'] = $table;
            $output['titulo'] = "Prueba";

            $this->output("crud",$output);
	}
        
        function output($vista = null, $data = null){
            
            $this->load->view('layout/header');
            if($vista){
                $this->load->view($vista,$data);    
            }
            $this->load->view('layout/footer');
        }
}
